//
//  ChatViewController.h
//  GuardianAngel
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 GuardianAngel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ChatViewController : UIViewController

@property (strong, nonatomic) PFObject *issue;

@end

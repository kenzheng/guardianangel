//
//  SelectPhotoViewController.h
//  Thrive
//
//  Created by Ken Zheng on 9/11/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TesseractOCR/TesseractOCR.h>

@interface SelectPhotoViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, G8TesseractDelegate>

@end

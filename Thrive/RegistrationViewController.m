//
//  RegistrationViewController.m
//  Thrive
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import "RegistrationViewController.h"
#import <RestKit/RestKit.h>
#import "NetworkController.h"

#import <Parse/Parse.h>

@interface RegistrationViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tfUsername;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfDriverLicenseNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfMonth;
@property (weak, nonatomic) IBOutlet UITextField *tfDay;
@property (weak, nonatomic) IBOutlet UITextField *tfYear;

@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnRegisterPressed:(id)sender {
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" equalTo:self.tfUsername.text];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (object != nil) {
            NSLog(@"User exist");
            //login failed
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Failed"
                                                            message:@"Username already exists."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
        else
        {
            PFQuery *query = [PFQuery queryWithClassName:@"Citation"];
            [query whereKey:@"drivers_license_number" equalTo:self.tfDriverLicenseNumber.text];
            [query whereKey:@"defendant_address" equalTo:self.tfAddress.text];
            
            NSString *dateStr = [NSString stringWithFormat:@"%@/%@/%@ 0:00", self.tfMonth.text, self.tfDay.text, self.tfYear.text];
//            NSCalendar *calendar = [NSCalendar currentCalendar];
//            NSDateComponents *components = [[NSDateComponents alloc] init];
//            
//            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//            f.numberStyle = NSNumberFormatterDecimalStyle;
//            NSNumber *day = [f numberFromString:self.tfDay.text];
//            NSNumber *month = [f numberFromString:self.tfMonth.text];
//            NSNumber *year = [f numberFromString:self.tfYear.text];
//            
//            [components setDay:[day longValue]];
//            [components setMonth:[month longValue]];
//            [components setYear:[year longValue]];
//            NSDate *date = [calendar dateFromComponents:components];
            [query whereKey:@"date_of_birth" equalTo:dateStr];
            NSArray *arr = [query findObjects];
            if ([arr count] == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Failed"
                                                                message:@"Your credentials don't match up with the system's information."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                return;
            } else {
                PFObject *user = [PFObject objectWithClassName:@"User"];
                user[@"email"] = self.tfUsername.text;
                user[@"password"] = self.tfPassword.text;
                user[@"driversLicense"] = self.tfDriverLicenseNumber.text;
                user[@"phoneNumber"] = self.tfPhoneNumber.text;
                user[@"dob"] = dateStr;
                user[@"address"] = self.tfAddress.text;
                
                [user saveInBackground];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Complete"
                                                                message:@"Welcome to Thrive!"
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"OK!", nil];
                alert.tag = 101;
                [alert show];
            }
            

        }
    }];
    
    
}

- (IBAction)btnCancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertView.tag==101)
    {
        if (buttonIndex == 0)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

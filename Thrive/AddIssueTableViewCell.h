//
//  AddIssueTableViewCell.h
//  GuardianAngel
//
//  Created by Ken Zheng on 9/13/15.
//  Copyright (c) 2015 GuardianAngel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddIssueTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblBody;

@end

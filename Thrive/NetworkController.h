//
//  NetworkController.h
//  Thrive
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import <Parse/Parse.h>

@interface NetworkController : NSObject

+(id) sharedInstance;

@property NSManagedObjectContext * managedObjectContext;
@property (nonatomic, strong) RKManagedObjectStore *managedObjectStore;

-(PFObject *) getLoggedInUser;

@end

//
//  IssueListingTableViewCell.h
//  Thrive
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IssueListingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblTimestamp;


@end

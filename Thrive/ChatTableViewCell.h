//
//  ChatTableViewCell.h
//  GuardianAngel
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 GuardianAngel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblTimestamp;
@property (weak, nonatomic) IBOutlet UITextView *tvBody;
@property (weak, nonatomic) IBOutlet UIImageView *ivMaps;


@end

//
//  SelectPhotoViewController.m
//  Thrive
//
//  Created by Ken Zheng on 9/11/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import "SelectPhotoViewController.h"

@interface SelectPhotoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *ivSelectedImage;
@property (weak, nonatomic) IBOutlet UIButton *btnTakePhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnUseExistingPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnConvertToText;
@property (strong, nonatomic) G8Tesseract *tesseract;
@end

@implementation SelectPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tesseract = [[G8Tesseract alloc] initWithLanguage:@"eng"];
    self.tesseract.maximumRecognitionTime = 2.0;
    self.tesseract.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnTakePhotoPressed:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)btnUseExistingPhotoPressed:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.ivSelectedImage.image = chosenImage;
    self.tesseract.image = self.ivSelectedImage.image;
    NSLog(@"%@", [self.tesseract recognizedText]);

    [self.tesseract recognize];

    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)btnConvertToTextPressed:(id)sender {
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

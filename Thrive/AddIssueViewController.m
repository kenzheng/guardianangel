//
//  AddIssueViewController.m
//  Thrive
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import "AddIssueViewController.h"
#import <Parse/Parse.h>
#import "NetworkController.h"
#import <CoreLocation/CoreLocation.h>
#import "ChatViewController.h"
#import "AddIssueTableViewCell.h"


@interface AddIssueViewController () <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSNumber *numLat;
@property (strong, nonatomic) NSNumber *numLong;


@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *arrIssueStrings;


@end

@implementation AddIssueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    
    
    NSString * strTrafficViolation = @"Traffic Violation";
    NSString * strPlateExpiration = @"Plate Expiration";
    NSString * strCourt = @"Court";
    NSString * strPolice = @"Police";

    self.arrIssueStrings = [[NSMutableArray alloc] init];
    [self.arrIssueStrings addObject:strTrafficViolation];
    [self.arrIssueStrings addObject:strPlateExpiration];
    [self.arrIssueStrings addObject:strCourt];
    [self.arrIssueStrings addObject:strPolice];

    
    [self.tableView registerNib:[UINib nibWithNibName:@"AddIssueTableViewCell"
                                                                                    bundle:nil]
                                              forCellReuseIdentifier:@"AddIssueTableViewCell"];
    
    [self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:@"AddIssueTableViewCell"
                                                     bundle:nil]
               forCellReuseIdentifier:@"AddIssueTableViewCell"];
    
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc {
    [self.locationManager stopUpdatingLocation];
}

- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.arrIssueStrings count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddIssueTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AddIssueTableViewCell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[AddIssueTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddIssueTableViewCell"];
    }
    [cell.lblBody setText:[self.arrIssueStrings objectAtIndex:indexPath.section]];

    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *str = [self.arrIssueStrings objectAtIndex:indexPath.section];
    [self performSegueWithIdentifier:@"search_issues_to_chat" sender:str];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PFObject *issue = [PFObject objectWithClassName:@"Issue"];
    PFRelation *userRelation = [issue relationForKey:@"user"];
    [userRelation addObject:[[NetworkController sharedInstance] getLoggedInUser]];
    issue[@"name"] = sender;
    issue[@"longitude"] = self.numLong;
    issue[@"latitude"] = self.numLat;
    [issue save];

    ChatViewController *viewController = [segue destinationViewController];
    viewController.issue = issue;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSString *longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        NSString *latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        self.numLong = [f numberFromString:longitude];
        self.numLat = [f numberFromString:latitude];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

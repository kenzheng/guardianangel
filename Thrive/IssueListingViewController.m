//
//  IssueListingViewController.m
//  Thrive
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import "IssueListingViewController.h"
#import "IssueListingTableViewCell.h"
#import <Parse/Parse.h>
#import "NetworkController.h"
#import "ChatViewController.h"

@interface IssueListingViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

@property (strong, nonatomic) NSMutableArray *arrIssues;

@end

@implementation IssueListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    self.arrIssues = [[NSMutableArray alloc] init];

}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
    
    PFQuery *issueQuery = [PFQuery queryWithClassName:@"Issue"];
//    [issueQuery orderByDescending:@"updatedAt"];
    [issueQuery addDescendingOrder:@"updatedAt"];
    [issueQuery whereKey:@"user" equalTo:[[NetworkController sharedInstance] getLoggedInUser]];
    self.arrIssues = [[NSMutableArray alloc] initWithArray:[issueQuery findObjects]];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnAddPressed:(id)sender {
    [self performSegueWithIdentifier:@"your_issues_to_search_issues" sender:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.arrIssues count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IssueListingTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"IssueListingTableViewCell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[IssueListingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IssueListingTableViewCell"];
    }
    PFObject *issue = [self.arrIssues objectAtIndex:indexPath.section];
    NSString *titleString = [issue objectForKey:@"name"];
    [cell.lblTitle setText:titleString];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSString *date = [formatter stringFromDate:[issue createdAt]];
    [cell.lblTimestamp setText:date];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PFObject *issue = [self.arrIssues objectAtIndex:indexPath.section];
    [self performSegueWithIdentifier:@"listing_to_chat" sender:issue];
}

- (IBAction)btnLogoutPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"email"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"listing_to_chat"]) {
        ChatViewController *chatController = [segue destinationViewController];
        chatController.issue = sender;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

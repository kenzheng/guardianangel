//
//  MapViewController.m
//  GuardianAngel
//
//  Created by Ken Zheng on 9/13/15.
//  Copyright (c) 2015 GuardianAngel. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>

@interface MapViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSNumber *latitude = self.issue[@"latitude"];
    NSNumber *longitude = self.issue[@"longitude"];

    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 400, 400)];
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
    point.coordinate = startCoord;
    point.title= @"Original Location";
    
    [self.mapView addAnnotation:point];
}

-(void) putPinsOnMap {
    //TODO:
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

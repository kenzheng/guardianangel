//
//  IssueListingTableViewCell.m
//  Thrive
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import "IssueListingTableViewCell.h"

@interface IssueListingTableViewCell ()


@end

@implementation IssueListingTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

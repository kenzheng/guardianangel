//
//  ChatViewController.m
//  GuardianAngel
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 GuardianAngel. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatTableViewCell.h"
#import "NetworkController.h"
#import "MapViewController.h"
#import "Stripe.h"

@interface ChatViewController () <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, PKPaymentAuthorizationViewControllerDelegate, PKPaymentAuthorizationViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITextView *tvChat;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vertSpaceChatBarToBottomSuperView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (strong, nonatomic) NSMutableArray *arrMessages;
@property (nonatomic,strong) UIActionSheet *pickerMenu;
@property (nonatomic,strong) UIImagePickerController *imagePickerController;

@end

@implementation ChatViewController
static NSString *kCameraText = @"Camera";
static NSString *kPhotoLibraryText = @"Photo Library";
static NSString *kCancelText = @"Cancel";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.tableView addGestureRecognizer:tapRecognizer];
    [self.viewHeader addGestureRecognizer:tapRecognizer];
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    
    self.arrMessages = [[NSMutableArray alloc] init];
    PFRelation *issueMessagesRelation =  [self.issue relationForKey:@"messages"];
    PFQuery *query = [issueMessagesRelation query];
    [query orderByAscending:@"createdAt"];
    self.arrMessages = [[NSMutableArray alloc]initWithArray: [query findObjects]];
    
    [self.tabBarController.tabBar setHidden:YES];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self scrollToEndWithAnimation:YES];
}

-(void) createMapCell {
        if ([self.arrMessages count] > 0) {
                PFObject *zeroMessage = [PFObject objectWithClassName:@"Message"];
                zeroMessage[@"body"] = @"map";
                PFRelation *userRelation = [zeroMessage relationForKey:@"user"];
                [userRelation addObject:[[NetworkController sharedInstance] getLoggedInUser]];
                PFRelation *issueRelation = [zeroMessage relationForKey:@"issue"];
                [issueRelation addObject:self.issue];
                [zeroMessage save];
                [self.arrMessages addObject:zeroMessage];
                
                PFRelation *issueMsgRelation = [self.issue relationForKey:@"messages"];
                [issueMsgRelation addObject:zeroMessage];
                [self.issue save];
        
    }
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSendPressed:(id)sender {
    if ([self.tvChat.text isEqualToString:@""]) {
        return;
    }
    
    PFObject *message = [PFObject objectWithClassName:@"Message"];
    message[@"body"] = self.tvChat.text;
    PFRelation *userRelation = [message relationForKey:@"user"];
    [userRelation addObject:[[NetworkController sharedInstance] getLoggedInUser]];
    PFRelation *issueRelation = [message relationForKey:@"issue"];
    [issueRelation addObject:self.issue];
    message[@"isFromSystem"] = [NSNumber numberWithBool:NO];
    [message save];
    
    PFRelation *issueMsgRelation = [self.issue relationForKey:@"messages"];
    [issueMsgRelation addObject:message];
    [self.issue save];
    
    [self.arrMessages addObject:message];
    
    [self systemResponseToInput:self.tvChat.text];
    
    [self.tableView reloadData];
    [self scrollToEndWithAnimation:YES];
    
    self.tvChat.text = @"";
}

-(void) systemResponseToInput:(NSString *)string {
    PFObject *message;
    if ([string rangeOfString:@"ticket" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        message = [self createBaseMessage];
        message[@"body"] = @"I can help! Please send the citation number in this format 'Citation:xxx'";
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    } else if ([string rangeOfString:@"citation" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        NSString *citation = [string stringByReplacingOccurrencesOfString:@"citation:" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *myNumber = [f numberFromString:citation];
        PFQuery *query = [PFQuery queryWithClassName:@"Citation"];
        [query whereKey:@"citation_number" equalTo:myNumber];
        
        PFObject *citationObject = [query getFirstObject];
        NSString *courtAddress = [NSString stringWithFormat:@"The court to resolve this ticket is located at: %@", citationObject[@"court_address"]];
        
        message = [self createBaseMessage];
        message[@"body"] = courtAddress;
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    } else if ([string rangeOfString:@"food" options:NSCaseInsensitiveSearch].location != NSNotFound ||
               [string rangeOfString:@"shelter" options:NSCaseInsensitiveSearch].location != NSNotFound ||
               [string rangeOfString:@"job" options:NSCaseInsensitiveSearch].location != NSNotFound ||
               [string rangeOfString:@"health" options:NSCaseInsensitiveSearch].location != NSNotFound){
        message = [self createBaseMessage];
        message[@"body"] = @"Please call the United Way Hotline at (211)";
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    } else if ([string rangeOfString:@"location" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        [self createMapCell];
    } else if ([string rangeOfString:@"warrant" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        message = [self createBaseMessage];
        message[@"body"] = @"I can look into that for you. Please enter the number like so: 'Violation:xxx'";
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    } else if ([string rangeOfString:@"violation" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        NSString *violation = [string stringByReplacingOccurrencesOfString:@"violation:" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];

        PFQuery *query = [PFQuery queryWithClassName:@"Violation"];
        [query whereKey:@"violation_number" equalTo:violation];
        
        PFObject *violationObject = [query getFirstObject];
        NSNumber *boolWarrant = violationObject[@"warrant_status"];
        NSString * returnString;
        if ([boolWarrant boolValue] == YES) {
            returnString = @"Unfortunately, there is an active warrant out there Ken. Please call Arch City Defenders at 855-724-2489.";
        } else {
            NSString *costStr = violationObject[@"court_cost"];
            NSString *costStr2 = violationObject[@"fine_amount"];
            costStr = [costStr stringByReplacingOccurrencesOfString:@"$" withString:@""];
            costStr2 = [costStr2 stringByReplacingOccurrencesOfString:@"$" withString:@""];
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *costFinal = [f numberFromString:costStr];
            NSNumber *costFinal2 = [f numberFromString:costStr2];
            
            float finalFloat = [costFinal floatValue] + [costFinal2 floatValue];
            NSNumber *sum = @(finalFloat);
            NSString *reason = violationObject[@"violation_description"];
            returnString = [NSString stringWithFormat:@"You're in the clear. But there's costs totaling %@ for %@", [sum stringValue], reason];
        }
        message = [self createBaseMessage];
        message[@"body"] = returnString;
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    } else if ([string rangeOfString:@"nervous" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        message = [self createBaseMessage];
        message[@"body"] = @"You should arrive 20 minutes early, wear business tire, and make sure to keep your phone on silent.";
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    } else if ([string rangeOfString:@"pay" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        PKPaymentRequest *request = [Stripe
                                     paymentRequestWithMerchantIdentifier:@"merchant.com.Thrive"];
        // Configure your request here.
        NSString *label = @"Submit payment for the ticket";
        NSDecimalNumber *amount = [NSDecimalNumber decimalNumberWithString:@"0.01"];
        request.paymentSummaryItems = @[
                                        [PKPaymentSummaryItem summaryItemWithLabel:label
                                                                            amount:amount]
                                        ];
        
        if ([Stripe canSubmitPaymentRequest:request]) {
            PKPaymentAuthorizationViewController *paymentController;
            paymentController = [[PKPaymentAuthorizationViewController alloc]
                                 initWithPaymentRequest:request];
            paymentController.delegate = self;
            [self presentViewController:paymentController animated:YES completion:nil];

        } else {
            // Show the user your own credit card form (see options 2 or 3)
        }
    } else if ([string rangeOfString:@"apple" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        message = [self createBaseMessage];
        message[@"body"] = @"Nice! Your payment is on its way!";
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    } else if ([string rangeOfString:@"traffic" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        message = [self createBaseMessage];
        message[@"body"] = @"I can check on that for you. May I do a search using your Driver's License Number?";
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    } else if ([string rangeOfString:@"alright" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        PFObject *object = [[NetworkController sharedInstance] getLoggedInUser];
        NSString *license = object[@"driversLicense"];

        PFQuery *query = [PFQuery queryWithClassName:@"Citation"];
        [query whereKey:@"drivers_license_number" equalTo:license];
        
        NSArray *arr = [query findObjects];
        NSString *arrCount = [NSString stringWithFormat:@"%lu", (unsigned long)[arr count]];
        NSString *courtAddress = [NSString stringWithFormat:@"%@ ticket(s) are outstanding", arrCount];
        
        message = [self createBaseMessage];
        message[@"body"] = courtAddress;
        message[@"isFromSystem"] = [NSNumber numberWithBool:YES];
    }

    if (message) {
        [message save];
        [self.arrMessages addObject:message];
        
        PFRelation *issueMsgRelation = [self.issue relationForKey:@"messages"];
        [issueMsgRelation addObject:message];
        [self.issue save];
    }
    
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus))completion {

    [self systemResponseToInput:@"apple"];
    [self.tableView reloadData];
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [self systemResponseToInput:@"apple"];
    [self.tableView reloadData];

    [self dismissViewControllerAnimated:YES completion:nil];
}



-(PFObject *) createBaseMessage {
    PFObject *message = [PFObject objectWithClassName:@"Message"];
    PFRelation *userRelation = [message relationForKey:@"user"];
    [userRelation addObject:[[NetworkController sharedInstance] getLoggedInUser]]; 
    PFRelation *issueRelation = [message relationForKey:@"issue"];
    [issueRelation addObject:self.issue];
    return message;
}

- (void)scrollToEndWithAnimation:(BOOL)animated
{
    [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentSize.height)];
}

- (void) hideKeyboard {
    [self.view endEditing:YES];
}


- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.arrMessages count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChatTableViewCell" forIndexPath:indexPath];
    [cell bringSubviewToFront:cell.tvBody];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    PFObject *msg = [self.arrMessages objectAtIndex:indexPath.section];
    if (cell == nil) {
        cell = [[ChatTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatTableViewCell"];
    }
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:[msg objectForKey:@"body"]];
    [cell.tvBody setAttributedText:[[NSAttributedString alloc] initWithString:@""]];
    [cell.tvBody setText:@""];
    [cell.tvBody setText:[msg objectForKey:@"body"]];

    [cell.tvBody setDataDetectorTypes:UIDataDetectorTypeAddress];
    [cell.tvBody setAttributedText:str];
    [cell.tvBody setDataDetectorTypes:UIDataDetectorTypeAddress];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSString *date = [formatter stringFromDate:[msg createdAt]];
    [cell.lblTimestamp setText:date];
    
    if (msg[@"isFromSystem"] == [NSNumber numberWithBool:0]) {
        //from user
        [cell.ivAvatar setImage:[UIImage imageNamed:@"kenAvatar"]];
    } else {
        //from system
        [cell.ivAvatar setImage:[UIImage imageNamed:@"botAvatar"]];
    }
    
    if ([cell.tvBody.text isEqualToString:@"map"]){
        [cell.tvBody setHidden:YES];
        [cell.lblTimestamp setHidden:YES];
        [cell.ivAvatar setHidden:YES];
        [cell.ivMaps setHidden:NO];
    }
    else {
        [cell.tvBody setHidden:NO];
        [cell.lblTimestamp setHidden:NO];
        [cell.ivAvatar setHidden:NO];
        [cell.ivMaps setHidden:YES];
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatTableViewCell * cell = (ChatTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    if (!cell.ivMaps.hidden) {
        [self performSegueWithIdentifier:@"chat_to_map" sender:nil];
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"chat_to_map"]) {
        MapViewController *controller = [segue destinationViewController];
        [controller setIssue:self.issue];
    }
}


//notified whenever keyboard appears. Sets instance variable to the keyboard height. Has the chat bar ride on top of the keyboard
-(void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    NSInteger keyboardHeight = keyboardFrame.size.height;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    self.vertSpaceChatBarToBottomSuperView.constant = keyboardHeight;
    
    [self.view layoutIfNeeded];
    [UIView commitAnimations];
    
    //enable the dismissal tap gesture recognizer
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    self.vertSpaceChatBarToBottomSuperView.constant = 0;
    
    [self.view layoutIfNeeded];
    [UIView commitAnimations];
}


@end

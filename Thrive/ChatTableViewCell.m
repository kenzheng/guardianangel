//
//  ChatTableViewCell.m
//  GuardianAngel
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 GuardianAngel. All rights reserved.
//

#import "ChatTableViewCell.h"

@implementation ChatTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

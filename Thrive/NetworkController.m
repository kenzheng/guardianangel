//
//  NetworkController.m
//  Thrive
//
//  Created by Ken Zheng on 9/12/15.
//  Copyright (c) 2015 Thrive. All rights reserved.
//

#import "NetworkController.h"

@interface NetworkController ()

@property (strong, nonatomic) PFObject *loggedInUser;

@end

@implementation NetworkController

+(id) sharedInstance {
    static NetworkController *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[NetworkController alloc] init];
    });
    
    return _sharedInstance;
}

-(PFObject *) getLoggedInUser {
    if (!self.loggedInUser) {
        PFQuery *query = [PFQuery queryWithClassName:@"User"];
        [query whereKey:@"email" equalTo:[[NSUserDefaults standardUserDefaults] objectForKey:@"email"]];
        
        self.loggedInUser = [query getFirstObject];
    }
    return self.loggedInUser;
}


- (instancetype)init
{
    if (self = [super init])
    {
        // Override Log level here if needed for this class
        
    }
    return self;
}

- (void)dealloc
{
}

@end
